#!/bin/bash

echo -e "$(hostname -i)\t$(hostname) $(hostname).localdomain" >> /etc/hosts
/etc/init.d/sendmail start

source /usr/local/bin/docker-entrypoint.sh

