This repository will host the docker image and kubernetes configuration for cuuats.org.

The contents of the website will be in `tank1/ds1/cuuats.org/wordpress` with related 
information about the database cuuats.org uses in `tank1/ds1/cuuats.org/mariadb`.